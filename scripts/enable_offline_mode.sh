#!/usr/bin/env sh

set -eu

# The purpose of this script is to simulate an offline environment. It ensures
# it can first reach the outside world by making a request to google.com. Then,
# using iptables, it disable external communication. Finally it checks that
# google.com can no longer be reached.

if command -v apt-get; then
  apt-get update
  apt-get install -y iptables curl
  update-alternatives --set iptables /usr/sbin/iptables-legacy
elif command -v microdnf; then
  microdnf -y update --disableplugin=subscription-manager
  microdnf -y upgrade --disableplugin=subscription-manager
  microdnf -y install --disableplugin=subscription-manager iptables curl
elif command -v yum; then
  yum -y -q update --disableplugin=subscription-manager
  yum -y -q upgrade --disableplugin=subscription-manager
  yum -y -q install --disableplugin=subscription-manager iptables curl
elif command -v apk; then
  apk upgrade
  apk --no-cache add iptables curl
else
  echo "Error: Offline mode has not been enabled. Neither apk, apt-get, microdnf nor yum are installed."
  exit 1
fi

if ! curl -s --head http://www.google.com/ | grep "200 OK" >/dev/null; then
  echo "Error: google.com is not accessible even though the offline line has not been enabled yet."
  exit 1
fi

iptables -P INPUT DROP
iptables -P OUTPUT DROP

if curl -s --head http://www.google.com/ | grep "200 OK" >/dev/null; then
  echo "Error: Offline mode has not been enabled. google.com is still accessible."
  exit 1
fi

echo "Success: Offline mode has been enabled."
